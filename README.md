# C# Common

A C# library with common functionality that extends the standard library.

## Contributing

See <https://github.com/egonolieux/personal-conventions>.

## Legal

See [LICENSE](LICENSE).
