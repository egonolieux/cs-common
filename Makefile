RELATIVE_CSPROJ_FILE_PATH = src/EgonOlieux.Common/EgonOlieux.Common.csproj
RELATIVE_BUILD_DIR_PATH = build

MAKEFLAGS += --always-make

.ONESHELL:
.SILENT:

version:
	echo '$(shell grep -oP '<Version>\K[^<]+' '$(RELATIVE_CSPROJ_FILE_PATH)')'

build:
	dotnet publish -c 'Release' -o '$(RELATIVE_BUILD_DIR_PATH)' '$(RELATIVE_CSPROJ_FILE_PATH)'

nuget-build:
	dotnet pack -c 'Release' -o '$(RELATIVE_BUILD_DIR_PATH)' '$(RELATIVE_CSPROJ_FILE_PATH)'

clean:
	rm -rf '$(RELATIVE_BUILD_DIR_PATH)'
