using System.Text.RegularExpressions;

namespace EgonOlieux.Common.Extensions;

public static partial class StringExtensions
{
    public static string? ToNullIfEmpty(this string? value) =>
        string.IsNullOrEmpty(value) ? null : value;

    public static string? ToNullIfWhiteSpace(this string? value) =>
        string.IsNullOrWhiteSpace(value) ? null : value;

    public static string ReplacePrefix(this string value, string oldPrefix, string newPrefix) =>
        ReplacePrefix(value, oldPrefix, newPrefix, StringComparison.CurrentCulture);

    public static string ReplacePrefix(
        this string value,
        string oldPrefix,
        string newPrefix,
        StringComparison stringComparison
    ) =>
        value.StartsWith(oldPrefix, stringComparison)
            ? newPrefix + value[oldPrefix.Length..]
            : value;

    public static string ReplaceSuffix(this string value, string oldSuffix, string newSuffix) =>
        ReplaceSuffix(value, oldSuffix, newSuffix, StringComparison.CurrentCulture);

    public static string ReplaceSuffix(
        this string value,
        string oldSuffix,
        string newSuffix,
        StringComparison stringComparison
    ) =>
        value.EndsWith(oldSuffix, stringComparison)
            ? value[..^oldSuffix.Length] + newSuffix
            : value;

    public static string FirstCharToLower(this string value) =>
        !string.IsNullOrEmpty(value)
            ? string.Concat(value[0].ToString().ToLower(), value.AsSpan(start: 1))
            : value;

    public static string FirstCharToUpper(this string value) =>
        !string.IsNullOrEmpty(value)
            ? string.Concat(value[0].ToString().ToUpper(), value.AsSpan(start: 1))
            : value;

    public static string DeduplicateSpaces(this string value) =>
        DuplicateSpacesGeneratedRegex().Replace(value, " ");

    /// <summary>
    /// <see cref="SplitEscaped(string, string, StringSplitOptions)"/>
    /// </summary>
    public static IList<string> SplitEscaped(this string value, char separator, StringSplitOptions options) =>
        value.SplitEscaped(separator.ToString(), options);

    /// <summary>
    /// Splits a string into substrings based on a specified delimiting character.
    /// Characters that conflict with the separator can be escaped by prefixing them with the "\" character.
    /// </summary>
    public static IList<string> SplitEscaped(this string value, string separator, StringSplitOptions options)
    {
        var values = new List<string>();

        var separatorPlaceholder = Guid
            .NewGuid()
            .ToString("N")
            .Replace(separator, string.Empty);

        foreach (
            var escapedValue in
            value
                .Replace($@"\{separator}", separatorPlaceholder)
                .Split(separator, options))
        {
            values.Add(escapedValue.Replace(separatorPlaceholder, separator));
        }

        return values;
    }

    public static IEnumerable<string> RemoveNullOrEmptyValues(this IEnumerable<string> values) =>
        values.Where(value => !string.IsNullOrEmpty(value));

    public static IEnumerable<string> RemoveNullOrWhiteSpaceValues(this IEnumerable<string> values) =>
        values.Where(value => !string.IsNullOrWhiteSpace(value));

    [GeneratedRegex(" {2,}")]
    private static partial Regex DuplicateSpacesGeneratedRegex();
}
