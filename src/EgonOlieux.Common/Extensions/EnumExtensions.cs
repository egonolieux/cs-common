using System.Reflection;
using System.Runtime.Serialization;

namespace EgonOlieux.Common.Extensions;

public static class EnumExtensions
{
    private static readonly Dictionary<Type, IDictionary<string, string>> EnumTypeToMemberNameToStringValueMap = [];

    /// <summary>
    /// Gets the string value of an enum value from the "[EnumMember]" attribute.
    /// If no such attribute is present, the default "ToString()" value is used.
    /// </summary>
    public static string GetStringValue<TEnum>(this TEnum enumValue)
        where TEnum : Enum =>
            GetStringValue(
                enumType: typeof(TEnum),
                enumMemberName: enumValue.ToString()
            );

    /// <summary>
    /// Converts a string value to an enum value.
    /// The string value is matched against the "[EnumMember]" attribute value.
    /// If no such attribute is present, the string value is matched against the default "ToString()" value.
    /// </summary>
    public static TEnum ToEnum<TEnum>(this string stringValue)
        where TEnum : struct, Enum =>
            TryToEnum<TEnum>(stringValue, out var enumValue)
                ? enumValue
                : throw new ArgumentException(
                    $"String \"{stringValue}\" couldn't be converted "
                        + $"to an enum value of type \"{typeof(TEnum).FullName}\".",
                    nameof(stringValue)
                );


    /// <summary>
    /// Tries to convert a string value to an enum value.
    /// The string value is matched against the "[EnumMember]" attribute value.
    /// If no such attribute is present, the string value is matched against the default "ToString()" value.
    /// </summary>
    public static bool TryToEnum<TEnum>(this string stringValue, out TEnum enumValue)
        where TEnum : struct, Enum
    {
        var enumType = typeof(TEnum);

        foreach (var enumMemberName in Enum.GetNames(enumType))
        {
            if (
                stringValue == GetStringValue(enumType, enumMemberName) ||
                stringValue == enumMemberName)
            {
                enumValue = (TEnum)Enum.Parse(enumType, enumMemberName);

                return true;
            }
        }

        enumValue = default;

        return false;
    }

    private static string GetStringValue(Type enumType, string enumMemberName)
    {
        EnumTypeToMemberNameToStringValueMap.TryAdd(enumType, new Dictionary<string, string>());

        EnumTypeToMemberNameToStringValueMap[enumType].TryAdd(
            enumMemberName,
            enumType
                .GetMember(enumMemberName)
                .FirstOrDefault()?
                .GetCustomAttribute<EnumMemberAttribute>(inherit: false)
                ?.Value
                    ?? enumMemberName
        );

        return EnumTypeToMemberNameToStringValueMap[enumType][enumMemberName];
    }
}
