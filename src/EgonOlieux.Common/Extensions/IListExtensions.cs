namespace EgonOlieux.Common.Extensions;

public static class IListExtensions
{
    public static IList<TValue>? ToNullIfEmpty<TValue>(this IList<TValue> values)
        where TValue : notnull =>
            (values.Count == 0) ? null : values;
}
