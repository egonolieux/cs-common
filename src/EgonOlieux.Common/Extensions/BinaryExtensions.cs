using System.Text;

namespace EgonOlieux.Common.Extensions;

public static class BinaryExtensions
{
    public static string ToHexString(this byte[] bytes, bool toUpperCase)
    {
        var stringBuilder = new StringBuilder(capacity: bytes.Length * 2);

        foreach (var byteValue in bytes)
        {
            stringBuilder.Append(byteValue.ToString(toUpperCase ? "X2" : "x2"));
        }

        return stringBuilder.ToString();
    }
}
