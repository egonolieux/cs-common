namespace EgonOlieux.Common.Exceptions;

public sealed class ValueNotFoundException : Exception
{
    public ValueNotFoundException()
    {
    }

    public ValueNotFoundException(string? message)
        : base(message)
    {
    }

    public ValueNotFoundException(string? message, Exception? innerException)
        : base(message, innerException)
    {
    }
}
