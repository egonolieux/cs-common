namespace EgonOlieux.Common.Exceptions;

public sealed class UnexpectedValueException : Exception
{
    public UnexpectedValueException()
    {
    }

    public UnexpectedValueException(string? message)
        : base(message)
    {
    }

    public UnexpectedValueException(string? message, Exception? innerException)
        : base(message, innerException)
    {
    }
}
