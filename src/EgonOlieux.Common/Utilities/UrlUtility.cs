﻿using System.Text;
using System.Web;

namespace EgonOlieux.Common.Utilities;

public static class UrlUtility
{
    public static string CombinePaths(params string[] urlPaths)
    {
        var stringBuilder = new StringBuilder();

        for (var i = 0; i < urlPaths.Length; i++)
        {
            var urlPath = urlPaths[i];

            if (i != 0)
            {
                urlPath = urlPath.TrimStart('/');
            }

            if (i + 1 != urlPaths.Length)
            {
                stringBuilder
                    .Append(urlPath.TrimEnd('/'))
                    .Append('/');
            }
            else
            {
                stringBuilder.Append(urlPath);
            }
        }

        return stringBuilder.ToString();
    }

    public static string ReplaceUrlQueryParam(string url, string key, string value) =>
        ReplaceUrlQueryParams(
            url,
            keyToValueMap: new Dictionary<string, string> { { key, value } }
        );

    public static string ReplaceUrlQueryParams(string url, IDictionary<string, string> keyToValueMap)
    {
        if (keyToValueMap.Count == 0)
        {
            return url;
        }

        var uriBuilder = new UriBuilder(url);
        var newKeyToValueMap = HttpUtility.ParseQueryString(uriBuilder.Query);

        foreach (var key in keyToValueMap.Keys)
        {
            newKeyToValueMap[key] = keyToValueMap[key];
        }

        uriBuilder.Query = newKeyToValueMap.ToString();

        return uriBuilder.Uri.AbsoluteUri;
    }

    public static string RemoveFragment(string url)
    {
        var urlFragmentIndex = url.IndexOf('#');

        return (urlFragmentIndex != -1)
            ? url[..urlFragmentIndex]
            : url;
    }
}
