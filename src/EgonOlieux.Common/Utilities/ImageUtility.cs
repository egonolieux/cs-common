using System.Collections.Immutable;

namespace EgonOlieux.Common.Utilities;

public static class ImageUtility
{
    public static readonly IReadOnlyDictionary<string, string> ImageFormatToFileExtensionMap =
        new Dictionary<string, string>
        {
            { "gif", ".gif" },
            { "jpeg", ".jpg" },
            { "png", ".png" },
        }.ToImmutableDictionary();

    public static readonly IReadOnlyList<byte> GifByteSignature = [
        0x47,
        0x49,
        0x46,
    ];

    public static readonly IReadOnlyList<byte> JpegByteSignature =
    [
        0xFF,
        0xD8,
        0xFF,
    ];

    public static readonly IReadOnlyList<byte> PngByteSignature =
    [
        0x89,
        0x50,
        0x4E,
        0x47,
        0x0D,
        0x0A,
        0x1A,
        0x0A,
    ];

    public static string? GuessFileExtension(IEnumerable<byte> imageBytes)
    {
        if (imageBytes.Take(GifByteSignature.Count).SequenceEqual(GifByteSignature))
        {
            return ImageFormatToFileExtensionMap["gif"];
        }

        if (imageBytes.Take(JpegByteSignature.Count).SequenceEqual(JpegByteSignature))
        {
            return ImageFormatToFileExtensionMap["jpeg"];
        }

        if (imageBytes.Take(PngByteSignature.Count).SequenceEqual(PngByteSignature))
        {
            return ImageFormatToFileExtensionMap["png"];
        }

        return null;
    }
}
