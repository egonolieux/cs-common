namespace EgonOlieux.Common.Events;

public sealed class ErrorOccurredEventArgs(
    string message,
    Exception? exception = null
)
    : EventArgs
{
    public string Message { get; } = message;

    public Exception? Exception { get; } = exception;
}
