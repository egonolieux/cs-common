namespace EgonOlieux.Common.Events;

public sealed class WarningOccurredEventArgs(string message) : EventArgs
{
    public string Message { get; } = message;
}
